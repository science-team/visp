#!/usr/bin/make -f
# -*- makefile -*-

# Hardening flags.
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

BUILDDIR = obj-$(DEB_HOST_MULTIARCH)

# Retrieve environment information.
DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
DEB_HOST_ARCH_OS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_OS)

# Linux-specific stuff (dc1394, v4l, ois only available on linux)
ifeq ($(DEB_HOST_ARCH_OS),linux)
  CMAKE_ARCH_FLAGS = -DUSE_DC1394=ON -DUSE_V4L2=ON -DUSE_OIS=ON
else
  CMAKE_ARCH_FLAGS = -DUSE_DC1394=OFF -DUSE_V4L2=OFF -DUSE_OIS=OFF
endif

# On armel, since 3.4.0-3 build returns the following error
# "ABORT: Received TERM signal"
# Disable parallel build (make -j1) to reduce memory usage
ifeq ($(DEB_HOST_ARCH_OS),armel)
  export DEB_BUILD_MAINT_OPTIONS="parallel=1"
endif

# CMake flags
CMAKE_FLAGS = \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_VERBOSE_MAKEFILE=ON \
	-DCMAKE_C_FLAGS_RELEASE="$(CFLAGS)" \
	-DCMAKE_CXX_FLAGS_RELEASE="$(CXXFLAGS)" \
	-DCMAKE_SHARED_LINKER_FLAGS_RELEASE="$(LDFLAGS)" \
	-DCMAKE_EXE_LINKER_FLAGS_RELEASE="$(LDFLAGS)" \
	-DCMAKE_INSTALL_LIBDIR="lib" \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_SKIP_RPATH=ON \
        -DENABLE_MULTIARCH=ON \
        -DUSE_SOQT=OFF \
	$(CMAKE_ARCH_FLAGS)

%:
	dh $@ --buildsystem=cmake

override_dh_clean:
	dh_clean -O--buildsystem=cmake

override_dh_auto_configure:
	dh_auto_configure -- $(CMAKE_FLAGS)

override_dh_auto_build-indep:
	dh_auto_build -- visp_doc # Generate Doxygen HTML documentation.

override_dh_installdocs:
	dh_installdocs -X.md5

# Due to numerical imprecision, some tests are failing on ia64.
# This is not a critical issue so we let the testing fail on this
# architecture for now.
# See http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=723803
ifeq ($(DEB_HOST_ARCH),ia64)
  DH_AUTOTEST_CAN_FAIL=true
else
  DH_AUTOTEST_CAN_FAIL=false
endif

override_dh_auto_test-indep:

override_dh_auto_test-arch:
	-LD_LIBRARY_PATH=$(shell realpath $(BUILDDIR))/lib dh_auto_test || ${DH_AUTOTEST_CAN_FAIL}
