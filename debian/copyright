Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ViSP
Upstream-Contact: visp@inria.fr
Source: https://visp.inria.fr/download

Files: *
Copyright: 2005-2023, Inria
License: GPL-2

Files: cmake/GNUInstallDirs.cmake
Copyright: 2011, Nikita Krupen'ko <krnekit@gmail.com>
License: BSD-3-clause

Files: modules/io/include/visp3/io/vpParseArgv.h
Copyright: 1989-1992 Regents of the University of California
License: Tcl/Tk-license

Files: modules/io/src/tools/vpParseArgv.cpp
Copyright: 1990 Regents of the University of California
License: Tcl/Tk-license

Files: modules/core/src/image/private/Font.hpp
Copyright: 2011-2017 Yermalayeu Ihar
License: MIT-license

Files: example/servo-franka/*
Copyright: 2017 Franka Emika GmbH
License: Apache-2.0

Files: 3rdparty/apriltag/*
Copyright: 2013-2016, The Regents of The University of Michigan.
License: BSD-2-clause

Files: 3rdparty/atidaq/*
Copyright: 1998, 1999 Thai Open Source Software Center Ltd
License: MIT-license

Files: 3rdparty/catch2/*
Copyright: 2019 Two Blue Cubes Ltd
License: Boost-software-license

Files: 3rdparty/clapack/*
Copyright: 1992-2008 The University of Tennessee.
License: BSD-3-clause

Files: 3rdparty/clipper/*
Copyright: Angus Johnson 2010-2017
License: Boost-software-license

Files: 3rdparty/pthreads4w/*
Copyright: 1999-2018, Pthreads4w contributors
License: Apache-2.0

Files: 3rdparty/pugixml-1.9/*
Copyright: 2006-2018, Arseny Kapoulkine
License: MIT-license

Files: 3rdparty/qbdevice/*
Copyright: 2016-2018, qbrobotics
License: BSD-3-clause

Files: 3rdparty/reflex-takktile2/*
Copyright: 2017 Open Source Robotics Foundation, Inc.
License: Apache-2.0

Files: 3rdparty/simdlib/*
Copyright: 2017 Sean Barrett
License: MIT-license

Files: 3rdparty/stb_image/*
Copyright: 2011-2017 Ihar Yermalayeu
License: MIT-license

Files: debian/*
Copyright: 2015-2023, Fabien Spindler <Fabien.Spindler@inria.fr>
License: GPL-2

License: Apache-2.0
 On Debian systems, the full text of the Apache License version 2.0 can be f>
 in the file '/usr/share/common-licenses/Apache-2.0'.

License: GPL-2
 See `/usr/share/common-licenses/GPL-2'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 3. Neither the name of the copyright holders nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Tcl/Tk-license
 Copyright 1990 Regents of the University of California
 Permission to use, copy, modify, and distribute this
 software and its documentation for any purpose and without
 fee is hereby granted, provided that the above copyright
 notice appear in all copies.  The University of California
 makes no representations about the suitability of this
 software for any purpose.  It is provided "as is" without
 express or implied warranty.

License: Boost-software-license
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: MIT-license
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 . 
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 . 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.


